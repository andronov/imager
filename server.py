import asyncio
import base64
import numpy
from PIL import Image
from aiohttp import web, WSMsgType


def base64ToString(b):
    return base64.b64decode(b).decode('utf-8')

async def handle(request):
    return web.FileResponse('index.html')

async def websocket_handler(request):

    ws = web.WebSocketResponse()
    await ws.prepare(request)

    async for msg in ws:
        if msg.type == WSMsgType.TEXT:
            if msg.data == 'close':
                await ws.close()
            else:
                if msg.data == 'get_image':
                    #im = Image.open('dog.jpg')
                    #pixels = list(im.getdata())
                    #x = numpy.array(pixels)
                    #print(len(x.tobytes()))
                    #await ws.send_bytes(x.tobytes())
                    continue
                    #im = Image.open('dog.jpg')
                    #pixels = list(im.getdata())
                    #await ws.send_bytes(im.tobytes())
                    #continue
                    with open("dog.jpg", "rb") as image_file:
                        content = image_file.read()
                        print(len(content))
                        encoded_data = base64.b64encode(content).decode('utf8')
                        print(len(encoded_data))
                        await ws.send_bytes(content)
                        #await ws.send_str(encoded_data)
        elif msg.type == WSMsgType.ERROR:
            print('ws connection closed with exception %s' %
                  ws.exception())

    print('websocket connection closed')

    return ws

async def handle_file(request):
    name = request.match_info.get('name')
    return web.FileResponse(name)


async def handle_pixels(request):
    im = Image.open('dog.jpg')
    pixels = list(im.getdata())
    return web.json_response(pixels)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()

    app = web.Application()
    app.router.add_get('/', handle)
    app.router.add_get('/pixels', handle_pixels)
    app.router.add_get('/file/{name}', handle_file)
    app.router.add_get('/ws', websocket_handler)
    web.run_app(app, port=8060, host='127.0.0.1')
