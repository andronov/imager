import base64
import os

import sys
from itertools import groupby, product

import io
import numpy
from PIL import Image


def main():
    """
    test
    Изображение 100 x 100 пикс. состоит из 10 000 пикселей
    10 000 пикс. x 4 Б = 40 000 Б
    40 000 Б / 1024 = 39 КБ
    Размер изображения - это сумма пикселей, умноженная на число байтов, используемых для кодирования каждого пикселя.
    Оптимизация изображения сводится к уменьшения этих двух составляющих.
    Вебсокет загрузка изображения, типа blur быстрый.
    :return:
    """

    #colors = list(product(range(256), repeat=3))
    #print(colors[:5])
    #return
    #  HEIF(apple)
    #  webp 25-34% smaller than jpeg
    #  58,5 kB (58 451 bytes)
    #  (350, 280)
    im = Image.open('dog.jpg')

    print(os.stat('dog.jpg').st_size)
    # print(len(im.histogram()))

    pixels = list(im.getdata())
    # print(pixels)
    print('pixels: ', len(pixels))
    print(sys.getsizeof(len(pixels)))
    #print('pixels: ', pixels)
    print(im.size)
    #print(numpy.asarray(im))

    r, g, b = im.getpixel((1, 1))
    print('rgb: ', r, g, b)
    #  16 777 216 комбинаций цветов

    def get_duplicates(arr):
        dup_arr = arr[:]
        for i in set(arr):
            dup_arr.remove(i)
        return list(set(dup_arr))

    #print(pixels)
    lists = [i for i in pixels]
    #print(lists)
    print(sys.getsizeof(len(lists)))
    """
    result = dict([(r, len(list(grp))) for r, grp in groupby(pixels)])
    print(result)
    z = 0
    for i, key in result.items():
        if key > 1:
            z += 1
            print(key)
    #print(len(get_duplicates(pixels)))
    print(z)
    """
    #print('p', im.tobytes())

    with open('dog.jpg', "rb") as imageFile:
        res = base64.b64encode(imageFile.read())
        print('res: ', len(res))
        #print(bytes(res))

    #pixels = pixels[0:len(pixels)-100000]
    #pixels = [(1, 8, 5) for i in range(len(pixels))]
    print(pixels[0:5])
    x = numpy.array(pixels)
    print(len(x))

    # imgByteArr = io.BytesIO()
    img = Image.new('RGB', im.size)
    img.putdata(pixels)
    img.save('test.jpg')

    # print(imgByteArr.getvalue())
    #  23,4 kB (23 404 bytes)

if __name__ == '__main__':
    main()
